import testinfra

def test_os_release(host):
	assert host.file("/etc/os-release").contains("Alpine")

def test_nginx_inactive(host):
	assert host.service("nginx").is_running is False

def test_nginx_version(host):
	assert host.package("nginx").version.startswith("1.16")
